<?php
/*
 * Copyright (c) 2023. Alexandr Verin.
 *
 * About us https://t.me/verin86
 */

namespace Verin\Vtiger;

use AllowDynamicProperties;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Verin\Vtiger\Interfaces\ToConvert;
use Verin\Vtiger\Traits\Account;
use Verin\Vtiger\Traits\AssignedUser;
use Verin\Vtiger\Traits\Contact;
use Verin\Vtiger\Traits\CreatedUser;
use Verin\Vtiger\Traits\ModifiedBy;

#[AllowDynamicProperties] abstract class ElementMap implements ToConvert
{
    use Account;
    use AssignedUser;
    use Contact;
    use CreatedUser;
    use ModifiedBy;

    public function __construct(
        public readonly string $createdtime,
        public string $modifiedtime,
        public readonly ?string  $id,
    ) {
    }

    protected static function getDataConfigKey(string $elementName, string $key, array $data): mixed
    {
        return $data[config("vtiger.elementTypes.$elementName.fields.$key")] ?? null;
    }

    abstract public static function elementType(): string;
    abstract public static function make(array $data);

    public function getCreatedTime(string $format = 'd.m.Y H:i'): string
    {
        return Carbon::parse($this->createdtime)->format($format);
    }

    public function getModifiedTime(string $format = 'd.m.Y H:i'): string
    {
        return Carbon::parse($this->modifiedtime)->format($format);
    }

    public function toValue(string $name): ?string
    {
        return $this->toCollect()->get($name);
    }

    public function toArray(): array
    {
        return (array) $this;
    }

    public function toJson(): false|string
    {
        return json_encode($this);
    }

    public function toCollect(): Collection
    {
        return collect($this->toArray());
    }
}
