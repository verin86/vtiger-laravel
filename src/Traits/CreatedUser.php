<?php
/*
 * Copyright (c) 2023. Alexandr Verin.
 *
 * About us https://t.me/verin86
 */

namespace Verin\Vtiger\Traits;

use GuzzleHttp\Exception\GuzzleException;
use Verin\Vtiger\Elements\UsersElementMap;
use Verin\Vtiger\Vtiger;

trait CreatedUser
{
    protected string $createdUser = 'created_user_id';

    /**
     * @throws GuzzleException
     */
    public function getCreatedUser(): false|UsersElementMap
    {
        return Vtiger::getInstance()->retrieveElementId(self::toValue($this->createdUser));
    }
}
