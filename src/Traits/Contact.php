<?php
/*
 * Copyright (c) 2023. Alexandr Verin.
 *
 * About us https://t.me/verin86
 */

namespace Verin\Vtiger\Traits;

use GuzzleHttp\Exception\GuzzleException;
use Verin\Vtiger\Elements\ContactsElementMap;
use Verin\Vtiger\Vtiger;

trait Contact
{
    protected string $contact = 'contact_id';

    /**
     * @throws GuzzleException
     */
    public function getContact(): false|ContactsElementMap
    {
        return Vtiger::getInstance()->retrieveElementId(self::toValue($this->contact));
    }
}
