<?php
/*
 * Copyright (c) 2023. Alexandr Verin.
 *
 * About us https://t.me/verin86
 */

namespace Verin\Vtiger\Traits;

use GuzzleHttp\Exception\GuzzleException;
use Verin\Vtiger\Elements\AccountsElementMap;
use Verin\Vtiger\Vtiger;

trait Account
{
    protected string $account = 'account_id';

    /**
     * @throws GuzzleException
     */
    public function getAccount(): false|AccountsElementMap
    {
        return Vtiger::getInstance()->retrieveElementId(self::toValue($this->account));
    }
}
