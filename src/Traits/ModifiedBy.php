<?php
/*
 * Copyright (c) 2023. Alexandr Verin.
 *
 * About us https://t.me/verin86
 */

namespace Verin\Vtiger\Traits;

use GuzzleHttp\Exception\GuzzleException;
use Verin\Vtiger\Elements\UsersElementMap;
use Verin\Vtiger\Vtiger;

trait ModifiedBy
{
    protected string $modifiedBy = 'modifiedby';

    /**
     * @throws GuzzleException
     */
    public function getModifiedBy(): false|UsersElementMap
    {
        return Vtiger::getInstance()->retrieveElementId(self::toValue($this->modifiedBy));
    }
}
