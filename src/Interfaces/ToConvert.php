<?php
/*
 * Copyright (c) 2023. Alexandr Verin.
 *
 * About us https://t.me/verin86
 */

namespace Verin\Vtiger\Interfaces;

use Illuminate\Support\Collection;

interface ToConvert
{
    public function toArray(): array;

    public function toJson(): false|string;

    public function toCollect(): Collection;

    public function toValue(string $name): ?string;
}
