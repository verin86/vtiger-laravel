<?php
/*
 * Copyright (c) 2023. Alexandr Verin.
 *
 * About us https://t.me/verin86
 */

namespace Verin\Vtiger\Enums\Accounts;

enum AccountTypes: string
{
    case Customer = 'Клиент';
    case Other = 'Другое';
    case Room = 'Помещение';
    case ResourceSupply = 'Ресурсоснабжение';
    case Maintenance = 'Обслуживание';
}
