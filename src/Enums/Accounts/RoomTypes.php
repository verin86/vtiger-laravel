<?php
/*
 * Copyright (c) 2023. Alexandr Verin.
 *
 * About us https://t.me/verin86
 */

namespace Verin\Vtiger\Enums\Accounts;

enum RoomTypes: string
{
    case Apartment = 'Квартира';
    case Pantry = 'Кладовая';
    case Office = 'Офис';
    case Stroller = 'Колясочная';
    case Parking = 'Паркинг';
}
