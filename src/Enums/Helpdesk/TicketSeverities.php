<?php
/*
 * Copyright (c) 2023. Alexandr Verin.
 *
 * About us https://t.me/verin86
 */

namespace Verin\Vtiger\Enums\Helpdesk;

enum TicketSeverities: string
{
    case Minor = 'Незначительная';
    case Major = 'Значительная';
    case Feature = 'Особенная';
    case Critical = 'Критичная';
}
