<?php
/*
 * Copyright (c) 2023. Alexandr Verin.
 *
 * About us https://t.me/verin86
 */

namespace Verin\Vtiger\Enums\Helpdesk;

enum TicketCategories: string
{
    case BigProblem = 'Большая проблема';
    case SmallProblem = 'Средняя проблема';
    case OtherProblem = 'Другая проблема';
}
