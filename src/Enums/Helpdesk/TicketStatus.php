<?php
/*
 * Copyright (c) 2023. Alexandr Verin.
 *
 * About us https://t.me/verin86
 */

namespace Verin\Vtiger\Enums\Helpdesk;

enum TicketStatus: string
{
    case Open = 'Открыто';
    case InProgress = 'В прогрессе';
    case WaitForResponse = 'Ожидание ответа';
    case Closed = 'Закрыто';
}
