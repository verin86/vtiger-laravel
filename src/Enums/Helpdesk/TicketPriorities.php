<?php
/*
 * Copyright (c) 2023. Alexandr Verin.
 *
 * About us https://t.me/verin86
 */

namespace Verin\Vtiger\Enums\Helpdesk;

enum TicketPriorities: string
{
    case Normal = 'Нормальный';
    case High = 'Высокий';
    case Urgent = 'Чрезвычайный';
}
