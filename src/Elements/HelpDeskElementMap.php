<?php
/*
 * Copyright (c) 2023. Alexandr Verin.
 *
 * About us https://t.me/verin86
 */

namespace Verin\Vtiger\Elements;

use Carbon\Carbon;
use Verin\Vtiger\ElementMap;
use Verin\Vtiger\Enums\Helpdesk\TicketCategories;
use Verin\Vtiger\Enums\Helpdesk\TicketPriorities;
use Verin\Vtiger\Enums\Helpdesk\TicketSeverities;
use Verin\Vtiger\Enums\Helpdesk\TicketStatus;

class HelpDeskElementMap extends ElementMap
{
    const ELEMENT_TYPE = 'HelpDesk';

    public function __construct(
        public readonly string $ticket_no,
        public readonly string $assigned_user_id,
        public readonly ?string $parent_id,
        public string $ticketpriorities,
        public string $ticketseverities,
        public string $ticketstatus,
        public string $ticketcategories,
        public ?string $hours,
        public ?string $days,
        public readonly string $createdtime,
        public string $modifiedtime,
        public ?bool $from_portal,
        public ?string $modifiedby,
        public string $ticket_title,
        public string $description,
        public ?string $solution,
        public string $contact_id,
        public readonly string $created_user_id,
        public ?string $source,
        public ?string $starred,
        public ?string $tags,
        public readonly ?string $id
    ) {
        parent::__construct($this->createdtime, $this->modifiedtime, $this->id);
    }

    public static function elementType(): string
    {
        return self::ELEMENT_TYPE;
    }

    public static function make(array $data): HelpDeskElementMap
    {
        return new self(
            $data['ticket_no'],
            $data['assigned_user_id'] ?? config('vtiger.assigned_user_id'),
            $data['parent_id'] ?? null,
            $data['ticketpriorities'] ?? TicketPriorities::High->name,
            $data['ticketseverities'] ?? TicketSeverities::Major->name,
            $data['ticketstatus'] ?? TicketStatus::Open->name,
            $data['ticketcategories'] ?? TicketCategories::BigProblem->name,
            $data['hours'] ?? null,
            $data['days'] ?? null,
            $data['createdtime'] ?? Carbon::now(),
            $data['modifiedtime'] ?? Carbon::now(),
            $data['from_portal'] ?? null,
            $data['modifiedby'] ?? null,
            $data['ticket_title'] ?? null,
            $data['description'] ?? null,
            $data['solution'] ?? null,
            $data['contact_id'] ?? null,
            $data['created_user_id'] ?? null,
            $data['source'] ?? null,
            $data['starred'] ?? null,
            $data['tags'] ?? null,
            $data['id'] ?? null
        );
    }

    public static function ticketStatus(string $status): string
    {
        $result = collect(TicketStatus::cases())->reduce(function ($data, $item) {
            $data[$item->name] = $item->value;

            return $data;
        }, []);

        return collect($result)->get($status);
    }

    public static function ticketPriorities(string $priority): string
    {
        $result = collect(TicketPriorities::cases())->reduce(function ($data, $item) {
            $data[$item->name] = $item->value;

            return $data;
        }, []);

        return collect($result)->get($priority);
    }

    public static function ticketSeverities(string $severity): string
    {
        $result = collect(TicketSeverities::cases())->reduce(function ($data, $item) {
            $data[$item->name] = $item->value;

            return $data;
        }, []);

        return collect($result)->get($severity);
    }

    public static function ticketCategories(string $category): string
    {
        $result = collect(TicketCategories::cases())->reduce(function ($data, $item) {
            $data[$item->name] = $item->value;

            return $data;
        }, []);

        return collect($result)->get($category);
    }

    public function getTicketStatus(): string
    {
        return self::ticketStatus($this->ticketstatus);
    }

    public function getTicketPriorities(): string
    {
        return self::ticketPriorities($this->ticketpriorities);
    }

    public function getTicketSeverities(): string
    {
        return self::ticketSeverities($this->ticketseverities);
    }

    public function getTicketCategories(): string
    {
        return self::ticketCategories($this->ticketcategories);
    }
}
