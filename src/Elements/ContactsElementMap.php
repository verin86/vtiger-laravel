<?php
/*
 * Copyright (c) 2023. Alexandr Verin.
 *
 * About us https://t.me/verin86
 */

namespace Verin\Vtiger\Elements;

use Verin\Vtiger\ElementMap;

class ContactsElementMap extends ElementMap
{
    const ELEMENT_TYPE = 'Contacts';

    public function __construct(
        public ?string $salutationtype,
        public ?string $firstname,
        public ?string $contact_no,
        public ?string $lastname,
        public ?string $mobile,
        public ?string $account_id,
        public readonly ?string $birthday,
        public ?string $email,
        public ?bool $donotcall,
        public ?string $emailoptout,
        public readonly ?string $assigned_user_id,
        public ?bool $notify_owner,
        public readonly string $createdtime,
        public string $modifiedtime,
        public ?string $modifiedby,
        public ?string $splastsms,
        public ?string $portal,
        public ?string $support_start_date,
        public ?string $support_end_date,
        public ?string $mailingstreet,
        public ?string $otherstreet,
        public ?string $mailingcity,
        public ?string $othercity,
        public ?string $mailingstate,
        public ?string $otherstate,
        public ?string $mailingzip,
        public ?string $otherzip,
        public ?string $mailingcountry,
        public ?string $othercountry,
        public ?string $imagename,
        public ?string $description,
        public ?string $vk_url,
        public ?bool $isconvertedfromlead,
        public readonly ?string $created_user_id,
        public ?string $source,
        public ?string $starred,
        public ?string $tags,
        public ?string $patronymic,
        public ?string $telegram_url,
        public readonly ?string $id,
    ) {
        parent::__construct($this->createdtime, $this->modifiedtime, $this->id);
    }

    public static function elementType(): string
    {
        return self::ELEMENT_TYPE;
    }

    public static function make(array $data): ContactsElementMap
    {
        return new self(
            $data['salutationtype'] ?? null,
            $data['firstname'] ?? null,
            $data['contact_no'] ?? null,
            $data['lastname'] ?? null,
            $data['mobile'] ?? null,
            $data['account_id'] ?? null,
            $data['birthday'] ?? null,
            $data['email'] ?? null,
            $data['donotcall'] ?? null,
            $data['emailoptout'] ?? null,
            $data['assigned_user_id'] ?? config('vtiger.assigned_user_id'),
            $data['notify_owner'] ?? null,
            $data['createdtime'] ?? null,
            $data['modifiedtime'] ?? null,
            $data['modifiedby'] ?? null,
            $data['splastsms'] ?? null,
            $data['portal'] ?? null,
            $data['support_start_date'] ?? null,
            $data['support_end_date'] ?? null,
            $data['mailingstreet'] ?? null,
            $data['otherstreet'] ?? null,
            $data['mailingcity'] ?? null,
            $data['othercity'] ?? null,
            $data['mailingstate'] ?? null,
            $data['otherstate'] ?? null,
            $data['mailingzip'] ?? null,
            $data['otherzip'] ?? null,
            $data['mailingcountry'] ?? null,
            $data['othercountry'] ?? null,
            $data['imagename'] ?? null,
            $data['description'] ?? null,
            $data['vk_url'] ?? null,
            $data['isconvertedfromlead'] ?? null,
            $data['created_user_id'] ?? null,
            $data['source'] ?? null,
            $data['starred'] ?? null,
            $data['tags'] ?? null,
            $data['patronymic'] ?? self::getDataConfigKey('Contacts', 'patronymic', $data),
            $data['telegram_url'] ?? self::getDataConfigKey('Contacts', 'telegram_url', $data),
            $data['id'] ?? null,
        );
    }

    public function getFullName(): string
    {
        return $this->patronymic ? $this->getFullNameLong() : $this->getFullNameShort();
    }

    public function getFullNameLong(): string
    {
        return "$this->firstname $this->patronymic ". substr($this->lastname, 0, 1).'.';
    }

    public function getFullNameShort(): string
    {
        return "$this->lastname $this->firstname";
    }

    public function getFullNameMiddle(): string
    {
        return "$this->lastname $this->firstname $this->patronymic";
    }

    public function toJson(): false|string
    {
        return $this->toCollect()
            ->except(['patronymic', 'telegram_url'])
            ->put(config('vtiger.elementTypes.Contacts.fields.patronymic'), $this->patronymic)
            ->put(config('vtiger.elementTypes.Contacts.fields.telegram_url'), $this->telegram_url)
            ->toJson();
    }
}
