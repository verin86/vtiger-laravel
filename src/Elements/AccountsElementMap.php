<?php
/*
 * Copyright (c) 2023. Alexandr Verin.
 *
 * About us https://t.me/verin86
 */

namespace Verin\Vtiger\Elements;

use Carbon\Carbon;
use Verin\Vtiger\ElementMap;
use Verin\Vtiger\Enums\Accounts\AccountTypes;
use Verin\Vtiger\Enums\Accounts\RoomTypes;

class AccountsElementMap extends ElementMap
{
    const ELEMENT_TYPE = 'Accounts';

    public function __construct(
        public readonly string $accountname,
        public readonly string $account_no,
        public readonly ?string $phone,
        public readonly ?string $otherphone,
        public readonly ?string $account_id,
        public readonly ?string $email1,
        public readonly ?string $accounttype,
        public readonly ?bool $emailoptout,
        public readonly ?string $assigned_user_id,
        public readonly string $createdtime,
        public string $modifiedtime,
        public readonly ?string $modifiedby,
        public readonly ?string $inn,
        public readonly ?string $kpp,
        public readonly ?string $splastsms,
        public readonly ?string $bill_street,
        public readonly ?string $ship_street,
        public readonly ?string $bill_city,
        public readonly ?string $ship_city,
        public readonly ?string $bill_state,
        public readonly ?string $ship_state,
        public readonly ?string $bill_code,
        public readonly ?string $ship_code,
        public readonly ?string $bill_country,
        public readonly ?string $ship_country,
        public readonly ?string $description,
        public readonly ?string $one_s_id,
        public readonly ?string $isconvertedfromlead,
        public readonly ?string $source,
        public readonly ?string $starred,
        public readonly ?string $tags,
        public readonly ?string $personal_account,
        public readonly ?string $room,
        public readonly ?string $room_area,
        public readonly ?string $room_type,
        public readonly ?string $floor,
        public readonly ?string $id
    ) {
        parent::__construct($this->createdtime, $this->modifiedtime, $this->id);
    }

    public static function elementType(): string
    {
        return self::ELEMENT_TYPE;
    }

    public static function make(array $data): AccountsElementMap
    {
        $data['personal_account'] = self::getDataConfigKey('Accounts', 'personal_account', $data);
        $data['room_area'] = self::getDataConfigKey('Accounts', 'area_room', $data);
        $data['room_type'] = self::getDataConfigKey('Accounts', 'type_room', $data);
        $data['floor'] = self::getDataConfigKey('Accounts', 'floor', $data);
        $data['room'] = (int) str_replace($data['floor'], '', $data['personal_account']);

        return new self(
            $data['accountname'] ?? null,
            $data['account_no'] ?? null,
            $data['phone'] ?? null,
            $data['otherphone'] ?? null,
            $data['account_id'] ?? null,
            $data['email1'] ?? null,
            $data['accounttype'] ?? null,
            $data['emailoptout'] ?? null,
            $data['assigned_user_id'] ?? config('vtiger.assigned_user_id'),
            $data['createdtime'] ?? Carbon::now(),
            $data['modifiedtime'] ?? Carbon::now(),
            $data['modifiedby'] ?? null,
            $data['inn'] ?? null,
            $data['kpp'] ?? null,
            $data['splastsms'] ?? null,
            $data['bill_street'] ?? null,
            $data['ship_street'] ?? null,
            $data['bill_city'] ?? null,
            $data['ship_city'] ?? null,
            $data['bill_state'] ?? null,
            $data['ship_state'] ?? null,
            $data['bill_code'] ?? null,
            $data['ship_code'] ?? null,
            $data['bill_country'] ?? null,
            $data['ship_country'] ?? null,
            $data['description'] ?? null,
            $data['one_s_id'] ?? null,
            $data['isconvertedfromlead'] ?? null,
            $data['source'] ?? null,
            $data['starred'] ?? null,
            $data['tags'] ?? null,
            $data['personal_account'] ?? null,
            $data['room'] ?? null,
            $data['room_area'] ?? null,
            $data['room_type'] ?? null,
            $data['floor'] ?? null,
            $data['id'] ?? null,
        );
    }

    public function getAccountTypes(): array
    {
        return AccountTypes::cases();
    }

    public function getRoomTypes(): array
    {
        return RoomTypes::cases();
    }

    public function getShipAddress(): string
    {
        return "$this->ship_code, $this->ship_state, $this->ship_city, $this->ship_street, $this->room";
    }

    public function getBillAddress(): string
    {
        return "$this->bill_code, $this->bill_state, $this->bill_city, $this->bill_street, $this->room";
    }

    public function toJson(): false|string
    {
        return $this->toCollect()
            ->except(['personal_account', 'room_area', 'room_type', 'floor'])
            ->put(config('vtiger.elementTypes.Accounts.fields.personal_account'), $this->personal_account)
            ->put(config('vtiger.elementTypes.Accounts.fields.room_area'), $this->room_area)
            ->put(config('vtiger.elementTypes.Accounts.fields.room_type'), $this->room_type)
            ->put(config('vtiger.elementTypes.Accounts.fields.floor'), $this->floor)
            ->toJson();
    }
}
