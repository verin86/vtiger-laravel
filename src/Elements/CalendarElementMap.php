<?php
/*
 * Copyright (c) 2023. Alexandr Verin.
 *
 * About us https://t.me/verin86
 */

namespace Verin\Vtiger\Elements;

use Carbon\Carbon;
use GuzzleHttp\Exception\GuzzleException;
use Verin\Vtiger\ElementMap;
use Verin\Vtiger\Vtiger;

class CalendarElementMap extends ElementMap
{
    const ELEMENT_TYPE = 'Calendar';

    public function __construct(
        public ?string $subject,
        public readonly string $assigned_user_id,
        public ?string $date_start,
        public ?string $time_start,
        public ?string $due_date,
        public ?string $parent_id,
        public ?string $contact_id,
        public ?string $taskstatus,
        public ?string $eventstatus,
        public ?string $taskpriority,
        public ?string $sendnotification,
        public readonly string $createdtime,
        public string $modifiedtime,
        public ?string $activitytype,
        public ?string $visibility,
        public ?string $description,
        public ?string $duration_hours,
        public ?string $duration_minutes,
        public ?string $location,
        public ?string $reminder_time,
        public ?string $recurringtype,
        public ?string $notime,
        public ?string $modifiedby,
        public readonly string $created_user_id,
        public ?string $source,
        public ?string $starred,
        public ?string $tags,
        public readonly ?string $id,
    ) {
        parent::__construct($this->createdtime, $this->modifiedtime, $this->id);
    }

    public static function elementType(): string
    {
        return self::ELEMENT_TYPE;
    }

    //todo not implemented: Act, Campaigns, Consignment, Leads, Potentials, Vendors,
    // Invoice, Quotes, PurchaseOrder, SalesOrder, Campaigns

    /**
     * Function to communicate with $parent_id return Accounts, HelpDesk.
     *
     *
     * @throws GuzzleException
     */
    public function getParentElementMap(): ElementMap|false
    {
        return Vtiger::getInstance()->retrieveElementId($this->parent_id);
    }

    public static function make(array $data): CalendarElementMap
    {
        return new self(
            $data['subject'] ?? null,
            $data['assigned_user_id'] ?? null,
            $data['date_start'] ?? null,
            $data['time_start'] ?? null,
            $data['due_date'] ?? null,
            $data['parent_id'] ?? null,
            $data['contact_id'] ?? null,
            $data['taskstatus'] ?? null,
            $data['eventstatus'] ?? null,
            $data['taskpriority'] ?? null,
            $data['sendnotification'] ?? null,
            $data['createdtime'] ?? Carbon::now(),
            $data['modifiedtime'] ?? Carbon::now(),
            $data['activitytype'] ?? null,
            $data['visibility'] ?? null,
            $data['description'] ?? null,
            $data['duration_hours'] ?? null,
            $data['duration_minutes'] ?? null,
            $data['location'] ?? null,
            $data['reminder_time'] ?? null,
            $data['recurringtype'] ?? null,
            $data['notime'] ?? null,
            $data['modifiedby'] ?? null,
            $data['created_user_id'] ?? null,
            $data['source'] ?? null,
            $data['starred'] ?? null,
            $data['tags'] ?? null,
            $data['id'] ?? null,
        );
    }
}
