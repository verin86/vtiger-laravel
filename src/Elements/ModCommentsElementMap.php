<?php
/*
 * Copyright (c) 2023. Alexandr Verin.
 *
 * About us https://t.me/verin86
 */

namespace Verin\Vtiger\Elements;

use Carbon\Carbon;
use GuzzleHttp\Exception\GuzzleException;
use Verin\Vtiger\ElementMap;
use Verin\Vtiger\Vtiger;

class ModCommentsElementMap extends ElementMap
{
    const ELEMENT_TYPE = 'ModComments';

    public function __construct(
        public readonly string $commentcontent,
        public ?string $source,
        public ?string $customer,
        public ?string $userid,
        public ?string $reasontoedit,
        public readonly string $creator,
        public string $assigned_user_id,
        public readonly string $createdtime,
        public string $modifiedtime,
        public ?string $related_to,
        public readonly ?string $parent_comments,
        public bool $is_private,
        public ?string $filename,
        public ?string $related_email_id,
        public readonly ?string $id,
    ) {
        parent::__construct($this->createdtime, $this->modifiedtime, $this->id);

        $this->contact = 'customer';
        $this->createdUser = 'creator';
    }

    public static function elementType(): string
    {
        return self::ELEMENT_TYPE;
    }

    public static function make(array $data): ModCommentsElementMap
    {
        return new self(
            $data['commentcontent'],
            $data['source'] ?? config('vtiger.source'),
            $data['customer'] ?? null,
            $data['userid'] ?? null,
            $data['reasontoedit'] ?? null,
            $data['creator'],
            $data['assigned_user_id'] ?? config('vtiger.assigned_user_id'),
            $data['createdtime'] ?? Carbon::now(),
            $data['modifiedtime'] ?? Carbon::now(),
            $data['related_to'] ?? null,
            $data['parent_comments'] ?? null,
            $data['is_private'] ?? false,
            $data['filename'] ?? 0,
            $data['related_email_id'] ?? 0,
            $data['id'] ?? null
        );
    }

    /**
     * @throws GuzzleException
     */
    public function getParentComment(): false|ModCommentsElementMap
    {
        return Vtiger::getInstance()->retrieveElementId($this->parent_comments);
    }

    /**
     *  Leads, Act, SalesOrder, PurchaseOrder, Quotes, Invoice, Faq, HelpDesk,
     *  Project,  ProjectTask, Potentials, Accounts,  Contacts, Consignment.
     *
     * @throws GuzzleException
     */
    public function getRelatedTo(): ElementMap|false
    {
        return Vtiger::getInstance()->retrieveElementId($this->related_to);
    }
}
