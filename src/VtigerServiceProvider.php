<?php
/*
 * Copyright (c) 2023. Alexandr Verin.
 *
 * About us https://t.me/verin86
 */

namespace Verin\Vtiger;

use Illuminate\Support\ServiceProvider;

class VtigerServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->publishes([
            __DIR__.'/../config/vtiger.php' => config_path('vtiger.php'),
        ], 'vtiger');

        // use the vendor configuration file as fallback
        $this->mergeConfigFrom(
            __DIR__.'/../config/vtiger.php', 'vtiger'
        );
    }

    public function register(): void
    {
        $this->app->bind(Vtiger::class, function () {
            return Vtiger::getInstance();
        });

        config([
            'config/vtiger.php',
        ]);
    }
}
