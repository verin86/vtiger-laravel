<?php
/*
 * Copyright (c) 2023. Alexandr Verin.
 *
 * About us https://t.me/verin86
 */

namespace Verin\Vtiger;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

/**
 * Laravel wrapper for the Vtiger API
 *
 * Class Vtiger
 */
class Vtiger
{
    protected string $url;

    protected string $username;

    protected string $accessKey;

    protected string $persistConnection;

    protected Client $guzzleClient;

    protected int $maxRetries;

    protected Collection $elementTypes;

    private static ?Vtiger $instance = null;

    protected array $authorize = [];

    private function elementTypeRefersClass(string $id): ?string
    {
        $elementName = array_key_first($this->elementTypes->where('id', $id)->toArray());

        return 'Verin\\Vtiger\\Elements\\'.$elementName.'ElementMap';
    }

    public static function getInstance(): self
    {
        if (! self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Vtiger constructor.
     * Obtain the connection information.
     * Initiate a new GuzzleHttp Client.
     */
    public function __construct()
    {
        $this->url = config('vtiger.url');
        $this->username = config('vtiger.username');
        $this->accessKey = config('vtiger.accesskey');
        $this->persistConnection = config('vtiger.persistconnection');
        $this->maxRetries = config('vtiger.max_retries');
        $this->elementTypes = collect(config('vtiger.elementTypes'));
        $this->guzzleClient = new Client(['http_errors' => false, 'verify' => false]);
    }

    protected function _processResponse($response): mixed
    {
        // decode the response
        if ($response->getStatusCode() == 200) {
            if (! empty($response->getBody()->getContents())) {
                $response->getBody()->rewind();
                $data = json_decode($response->getBody()->getContents());
            } else {
                $data = json_decode($response->getBody());
            }
        } else {
            $data = $response;
        }

        return $data;
    }

    /**
     * Get the session id for a login either from a stored session id or fresh from the API
     *
     *
     * @throws GuzzleException
     */
    protected function sessionId(): ?string
    {
        // Get the sessionData from the cache
        $sessionData = json_decode(Cache::get('vtiger_laravel'));

        if (! isset($sessionData, $sessionData->expireTime, $sessionData->token) ||
            $sessionData->expireTime < time() || empty($sessionData->token)) {
            $sessionData = $this->storeSession();
        }

        if (isset($sessionData->sessionid)) {
            return $sessionData->sessionid;
        }

        return $this->login($sessionData);
    }

    /**
     * Login to the VTiger API to get a new session
     *
     *
     * @throws GuzzleException
     * @throws Exception
     */
    protected function login($sessionData): ?string
    {
        $sessionId = null;
        $token = $sessionData->token;

        // Create unique key using combination of challenge token and access key
        $generatedKey = md5($token.$this->accessKey);

        $tryCounter = 1;

        do {
            // login using username and accessKey
            $response = $this->guzzleClient->request('POST', $this->url, [
                'form_params' => [
                    'operation' => 'login',
                    'username' => $this->username,
                    'accessKey' => $generatedKey,
                ],
            ]);

            // decode the response
            $loginResult = $this->_processResponse($response);
            $tryCounter++;
        } while (! isset($loginResult->success) && $tryCounter <= $this->maxRetries);

        if ($tryCounter >= $this->maxRetries) {
            throw new Exception('Could not complete login request within '.$this->maxRetries.' tries');
        }

        // Check if the response is invalid, or not successful.
        if ($response->getStatusCode() !== 200 || ! $loginResult->success) {
            if ($loginResult->error->code == 'INVALID_USER_CREDENTIALS' || $loginResult->error->code == 'INVALID_SESSIONID') {
                Cache::has('vtiger_laravel') && Cache::forget('vtiger_laravel');
            } else {
                $this->_processResponse($response);
            }
        } else {
            // Response is valid.
            $sessionId = $loginResult->result->sessionName;

            if (Cache::has('vtiger_laravel')) {
                $json = json_decode(Cache::pull('vtiger_laravel'));
                $json->sessionid = $sessionId;
                Cache::forever('vtiger_laravel', json_encode($json));
            } else {
                throw new Exception('Laravel cache key "vtiger_laravel" does not exist.');
            }
        }

        return $sessionId;
    }

    /**
     * Store a new session if needed
     *
     * @throws GuzzleException
     */
    protected function storeSession(): object
    {
        $updated = $this->getToken();
        $output = (object) $updated;

        Cache::forever('vtiger_laravel', json_encode($output));

        return $output;
    }

    /**
     * Get a new access token from the VTiger API
     *
     * @throws GuzzleException
     * @throws Exception
     */
    protected function getToken(): array
    {
        // perform API GET request
        $tryCounter = 1;

        do {
            $response = $this->guzzleClient->request('GET', $this->url, [
                'query' => [
                    'operation' => 'getchallenge',
                    'username' => $this->username,
                ],
            ]);

            $tryCounter++;
        } while (! isset($this->_processResponse($response)->success) && $tryCounter <= $this->maxRetries);

        if ($tryCounter >= $this->maxRetries) {
            throw new Exception('Could not complete get token request within '.$this->maxRetries.' tries');
        }

        // decode the response
        $challenge = $this->_processResponse($response);

        return [
            'token' => $challenge->result->token,
            'expireTime' => $challenge->result->expireTime,
        ];
    }

    /**
     * Logout from the VTiger API
     *
     *
     * @return bool|mixed
     *
     * @throws GuzzleException
     */
    protected function close(string $sessionId): mixed
    {
        if ($this->persistConnection) {
            return true;
        }

        // send a request to close current connection
        $response = $this->guzzleClient->request(
            'POST',
            $this->url,
            [
                'query' => [
                    'operation' => 'logout',
                    'sessionName' => $sessionId,
                ],
            ]
        );

        return $this->_processResponse($response);
    }

    /**
     * Override configured connection details.
     *
     * @return $this
     */
    public function auth(string $username, string $accessKey): static
    {
        $this->username = $username;
        $this->accessKey = $accessKey;

        return $this;
    }

    /**
     * Query the VTiger API with the given query string
     *
     *
     *
     * @throws GuzzleException
     */
    public function query(string $query): mixed
    {
        $sessionId = $this->sessionId();

        // send a request using a database query to get back any matching records
        $response = $this->guzzleClient->request('GET', $this->url, [
            'query' => [
                'operation' => 'query',
                'sessionName' => $sessionId,
                'query' => $query,
            ],
        ]);

        $this->close($sessionId);

        return $this->_processResponse($response);
    }

    /**
     * @throws GuzzleException
     */
    public function search(Builder $query, $quote = true)
    {
        $bindings = $query->getBindings();
        $queryString = $query->toSQL();

        foreach ($bindings as $binding) {
            if ($quote) {
                $queryString = preg_replace('/\?/', DB::connection()->getPdo()->quote($binding), $queryString, 1);
            } else {
                $queryString = preg_replace('/\?/', $binding, $queryString, 1);
            }
        }

        // In the event there is an offset, append it to the front of the limit
        // Vtiger does not support the offset keyword
        $matchOffset = [];
        $matchLimit = [];
        if (preg_match('/(\s[o][f][f][s][e][t]) (\d*)/', $queryString,
            $matchOffset) && preg_match('/(\s[l][i][m][i][t]) (\d*)/', $queryString, $matchLimit)) {
            $queryString = preg_replace('/(\s[o][f][f][s][e][t]) (\d*)/', '', $queryString);
            $queryString = preg_replace('/(\s[l][i][m][i][t]) (\d*)/', '', $queryString);
            $queryString = $queryString.' limit '.$matchOffset[2].','.$matchLimit[2];
        }

        // Remove the backticks and add semicolon
        $queryString = str_replace('`', '', $queryString).';';

        return $this->query($queryString);
    }

    /**
     * @throws GuzzleException
     */
    public function searchElements(Builder $query, $quote = true, bool $cacheUpdate = false): Collection
    {
        $result = collect();
        $cacheKey = 'vtiger:search:'.md5(str($query->toSql()));

        if (! $cacheUpdate && Cache::has($cacheKey)) {
            $search = Cache::get($cacheKey);
        } else {
            $search = $this->search($query, $quote);

            Cache::put($cacheKey, $search, config('vtiger.cache_time', 600));
        }

        if ($search->success) {

            foreach ($search->result as $search) {
                $id = explode('x', $search->id);

                if (! count($id)) {

                    continue;
                }

                $elementTypeRefersClass = $this->elementTypeRefersClass($id[0]);

                if (! $elementTypeRefersClass instanceof ElementMap) {

                    continue;
                }

                $result->add($elementTypeRefersClass::make((array) $search));
            }
        }

        return $result;
    }

    /**
     * @throws GuzzleException
     */
    public function listTypes()
    {
        $sessionId = $this->sessionId();

        // Lookup the data
        // send a request to retrieve all list types
        $response = $this->guzzleClient->request('GET', $this->url, [
            'query' => [
                'operation' => 'listtypes',
                'sessionName' => $sessionId,
            ],
        ]);

        $this->close($sessionId);

        return $this->_processResponse($response);
    }

    /**
     * Related types a record from the VTiger API
     * Format of $elem must be VtigerModuleEnum
     * send a request to related types a record
     *
     *
     * @throws GuzzleException
     */
    public function relatedTypes(ElementMap $elementMap): mixed
    {
        $sessionId = $this->sessionId();
        $response = $this->guzzleClient->request('GET', $this->url, [
            'query' => [
                'operation' => 'relatedtypes',
                'sessionName' => $sessionId,
                'elementType' => $elementMap::elementType(),
            ],
        ]);
        $this->close($sessionId);

        return $this->_processResponse($response);
    }

    /**
     * @throws GuzzleException
     */
    public function lookup($dataType, $value, $module, array $columns)
    {
        $sessionId = $this->sessionId();

        // Update columns into the proper format
        $columnsText = '';
        foreach ($columns as $column) {
            $columnsText .= '"'.$column.'",';
        }

        // Trim the last comma from the string
        $columnsText = substr($columnsText, 0, (strlen($columnsText) - 1));

        // Lookup the data
        // send a request to retrieve a record
        $response = $this->guzzleClient->request('GET', $this->url, [
            'query' => [
                'operation' => 'lookup',
                'sessionName' => $sessionId,
                'type' => $dataType,
                'value' => $value,
                'searchIn' => '{"'.$module.'":['.$columnsText.']}',
            ],
        ]);

        $this->close($sessionId);

        return $this->_processResponse($response);
    }

    /**
     * Retrieve a record from the VTiger API
     * Format of id must be {module_code}x{item_id}, e.g 4x12
     *
     *
     *
     * @throws GuzzleException
     */
    public function retrieve(string $id): mixed
    {
        $sessionId = $this->sessionId();

        // send a request to retrieve a record
        $response = $this->guzzleClient->request('GET', $this->url, [
            'query' => [
                'operation' => 'retrieve',
                'sessionName' => $sessionId,
                'id' => $id,
            ],
        ]);

        $this->close($sessionId);

        return $this->_processResponse($response);
    }

    /**
     *  Retrieve a record from the VTiger API
     *  Format of id must be {module_code}x{item_id}, e.g 4x12
     *
     *
     *
     * @param  bool  $cacheUpdate  - is true Cache reset
     *
     * @throws GuzzleException
     */
    public function retrieveElementId(string $id, bool $cacheUpdate = false): false|ElementMap
    {
        if (count($result = explode('x', $id)) !== 2) {
            return false;
        }

        if (! $cacheUpdate && Cache::has("vtiger:retrieve:$id")) {
            $retrieve = Cache::get("vtiger:retrieve:$id");
        } else {
            $retrieve = $this->retrieve($id);

            Cache::put("vtiger:retrieve:$id", $retrieve, config('vtiger.cache_time', 600));
        }

        $elementTypeRefersClass = $this->elementTypeRefersClass($result[0]);

        if (! $retrieve->success && ! $elementTypeRefersClass instanceof ElementMap) {

            return false;
        }

        return $elementTypeRefersClass::make((array) $retrieve->result);
    }

    /**
     * Create a new entry in the VTiger API
     * Make sure to fill all mandatory fields.
     *
     *
     * @throws GuzzleException
     */
    public function create(ElementMap $elementMap): mixed
    {
        $sessionId = $this->sessionId();

        $response = $this->guzzleClient->request('POST', $this->url, [
            'form_params' => [
                'operation' => 'create',
                'sessionName' => $sessionId,
                'element' => $elementMap->toJson(),
                'elementType' => $elementMap::elementType(),
            ],
        ]);

        $this->close($sessionId);

        return $this->_processResponse($response);
    }

    /**
     * Update an entry in the database from the given object
     * The object should be an object retreived from the database and then altered
     *
     *
     * @throws GuzzleException
     */
    public function update(ElementMap $elementMap): mixed
    {
        $sessionId = $this->sessionId();

        // send a request to update a record
        $response = $this->guzzleClient->request('POST', $this->url, [
            'form_params' => [
                'operation' => 'update',
                'sessionName' => $sessionId,
                'element' => $elementMap->toJson(),
            ],
        ]);

        $this->close($sessionId);

        return $this->_processResponse($response);
    }

    /**
     * The revise operation updates individual fields of a data object. Compared to an update operation
     * you send only the fields which needs to get changed.
     * However, the revise operation ignores unknown fields silently. A field could be unknown to a
     * certain user due to lack of permissions or by a wrong field name.
     *
     * @throws GuzzleException
     */
    public function revise(ElementMap $elementMap)
    {
        $sessionId = $this->sessionId();

        // send a request to revise a record
        $response = $this->guzzleClient->request('POST', $this->url, [
            'form_params' => [
                'operation' => 'revise',
                'sessionName' => $sessionId,
                'element' => $elementMap->toJson(),
            ],
        ]);

        $this->close($sessionId);

        return $this->_processResponse($response);
    }

    /**
     * Delete from the database using the given id
     * Format of id must be {module_id}x{item_id}, e.g 4x12
     *
     *
     *
     * @throws GuzzleException
     */
    public function delete(string $id): mixed
    {
        $sessionId = $this->sessionId();

        // send a request to delete a record
        $response = $this->guzzleClient->request('POST', $this->url, [
            'form_params' => [
                'operation' => 'delete',
                'sessionName' => $sessionId,
                'id' => $id,
            ],
        ]);

        $this->close($sessionId);

        return $this->_processResponse($response);
    }

    /**
     * Describe an element from the vTiger API from the given element name
     * send a request to describe a module (which returns a list of available fields) for a Vtiger module
     *
     *
     *
     * @throws GuzzleException
     */
    public function describe(ElementMap $elementMap): mixed
    {
        $sessionId = $this->sessionId();

        $response = $this->guzzleClient->request(
            'GET',
            $this->url,
            [
                'query' => [
                    'operation' => 'describe',
                    'sessionName' => $sessionId,
                    'elementType' => $elementMap::elementType(),
                ],
            ]
        );

        $this->close($sessionId);

        return $this->_processResponse($response);
    }
}
