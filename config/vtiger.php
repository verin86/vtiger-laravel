<?php
/*
 * Copyright (c) 2023. Alexandr Verin.
 *
 * About us https://t.me/verin86
 */

return [
    'url' => 'https://'.env('VTIGER_URL', config('app.')).'/vtigercrm/webservice.php',
    'username' => env('VTIGER_USERNAME', 'admin'),
    'accesskey' => env('VTIGER_KEY'),
    'persistconnection' => env('VTIGER_PERSISTENT', true),
    'max_retries' => env('VTIGER_RETRIES', 10),
    'cache_time' => env('VTIGER_CACHE_TIME', 600),

    'assigned_user_id' => env('VTIGER_ASSIGNED_USER_ID', '19x1'),
    'source' =>env('VTIGER_SOURCE', env('APP_NAME', 'laravel')),

    'elementTypes' => [
        'Calendar' => [
            'id' => 9,
            'fields' => [],
        ],
        'Leads' => [
            'id' => 10,
            'fields' => [],
        ],
        'Accounts' => [
            'id' => 11,
            'fields' => [
                'personal_account' => env('VTIGER_FIELDS_ACCOUNTS_PERSONAL_ACCOUNT'),
                'area_room' => env('VTIGER_FIELDS_ACCOUNTS_AREA_ROOM'),
                'type_room' => env('VTIGER_FIELDS_ACCOUNTS_TYPE_ROOM'),
                'floor' => env('VTIGER_FIELDS_ACCOUNTS_FLOOR'),
            ],
        ],
        'Contacts' => [
            'id' => 12,
            'fields' => [
                'patronymic' => env('VTIGER_FIELDS_CONTACTS_PATRONYMIC'),
                'telegram_url' => env('VTIGER_FIELDS_CONTACTS_TELEGRAM_URL'),
            ],
        ],
        'Documents' => [
            'id' => 15,
            'fields' => [],
        ],
        'HelpDesk' => [
            'id' => 17,
            'fields' => [],
        ],
        'Users' => [
            'id' => 19,
            'fields' => [],
        ],
        'ModComments' => [
            'id' => 31,
            'fields' => [],
        ],
        'Campaigns' => [
            'id' => null,
            'fields' => [],
        ],
        'Vendors' => [
            'id' => null,
            'fields' => [],
        ],
        'Faq' => [
            'id' => null,
            'fields' => [],
        ],
        'Quotes' => [
            'id' => null,
            'fields' => [],
        ],
        'PurchaseOrder' => [
            'id' => null,
            'fields' => [],
        ],
        'SalesOrder' => [
            'id' => null,
            'fields' => [],
        ],
        'Invoice' => [
            'id' => null,
            'fields' => [],
        ],
        'PriceBooks' => [
            'id' => null,
            'fields' => [],
        ],
        'Potentials' => [
            'id' => null,
            'fields' => [],
        ],
        'Products' => [
            'id' => null,
            'fields' => [],
        ],
        'Emails' => [
            'id' => null,
            'fields' => [],
        ],
        'Events' => [
            'id' => null,
            'fields' => [],
        ],
        'PBXManager' => [
            'id' => null,
            'fields' => [],
        ],
        'ServiceContracts' => [
            'id' => null,
            'fields' => [],
        ],
        'Services' => [
            'id' => null,
            'fields' => [],
        ],
        'Consignment' => [
            'id' => null,
            'fields' => [],
        ],
        'Act' => [
            'id' => null,
            'fields' => [],
        ],
        'SPSocialConnector' => [
            'id' => null,
            'fields' => [],
        ],
        'SPPayments' => [
            'id' => null,
            'fields' => [],
        ],
        'ProjectMilestone' => [
            'id' => null,
            'fields' => [],
        ],
        'ProjectTask' => [
            'id' => null,
            'fields' => [],
        ],
        'Project' => [
            'id' => null,
            'fields' => [],
        ],
        'Assets' => [
            'id' => null,
            'fields' => [],
        ],
        'Search' => [
            'id' => null,
            'fields' => [],
        ],
        'SMSNotifier' => [
            'id' => null,
            'fields' => [],
        ],
        'SPUnits' => [
            'id' => null,
            'fields' => [],
        ],
        'Groups' => [
            'id' => null,
            'fields' => [],
        ],
        'Currency' => [
            'id' => null,
            'fields' => [],
        ],
        'DocumentFolders' => [
            'id' => null,
            'fields' => [],
        ],
        'CompanyDetails' => [
            'id' => null,
            'fields' => [],
        ],
        'LineItem' => [
            'id' => null,
            'fields' => [],
        ],
        'Tax' => [
            'id' => null,
            'fields' => [],
        ],
        'ProductTaxes' => [
            'id' => null,
            'fields' => [],
        ],
    ],
];
